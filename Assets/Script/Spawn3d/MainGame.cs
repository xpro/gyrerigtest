﻿
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class MainGame : MonoBehaviour
{

    public GameObject prefabCube;
    public InputField fieldTime;
    public InputField fieldSpeed;
    public InputField fieldDistance;

    private float timeSpawn;
    private float speed;
    private float distance;



    void Start()
    {
        SetParatertCube();
        InvokeRepeating("SpawnNewObj", 0.0f,  this.timeSpawn);
    }

    public void Go()
    {
        SetParatertCube();
        CancelInvoke("SpawnNewObj");
        InvokeRepeating("SpawnNewObj", 0.0f,  this.timeSpawn);
    }

    private void SetParatertCube()
    {
        this.timeSpawn = Mathf.Clamp(float.Parse(fieldTime.text.Replace(",","."), CultureInfo.InvariantCulture.NumberFormat), 0.5f, 20f);
        fieldTime.text = this.timeSpawn.ToString().Replace(",",".");
        this.speed = Mathf.Clamp(float.Parse(fieldSpeed.text.Replace(",","."), CultureInfo.InvariantCulture.NumberFormat), 1f, 10f);
        fieldSpeed.text = this.speed.ToString().Replace(",",".");
        this.distance = Mathf.Clamp(float.Parse(fieldDistance.text.Replace(",","."), CultureInfo.InvariantCulture.NumberFormat), 10f, 50f);
        fieldDistance.text = this.distance.ToString().Replace(",",".");
;
    }

    private void SpawnNewObj()
    {
        Instantiate(prefabCube, new Vector3(-14, 0, 0), Quaternion.identity).GetComponent<CubeControl>().setDistance(this.distance).setSpeed(this.speed).Move();
    }
}
