using UnityEngine;

public class CubeControl : MonoBehaviour
{

    private Vector3 target;
    private float distance = 1.0f;
    private float speed = 5.0f;

    private bool move = false;

    public CubeControl setDistance(float distance)
    {
        this.distance = distance;
        target = transform.position + (new Vector3(1f,0,1f) * this.distance);
        return this;
    }

    public CubeControl setSpeed(float speed)
    {
        this.speed = speed;
        return this;
    }

    public void Move()
    {
        move = true;
    }

    void FixedUpdate()
    {
        if (move)
        {
            float step = this.speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            if (transform.position == target)
                Destroy(gameObject);
        }
    }
}
