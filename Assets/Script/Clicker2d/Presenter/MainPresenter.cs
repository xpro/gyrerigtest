using System.Collections;
using System.Collections.Generic;

public class MainPresenter
{

    private MainSceneView _viewMaiScene;
    private ClickModel _modelClick;
    public MainPresenter(MainSceneView view)
    {        
        _viewMaiScene = view;
        _modelClick = new ClickModel();
        displayCount();

    }

    public void displayCount()
    {
        _viewMaiScene.displayCount(_modelClick.CountClick);
    }

    public void addClick()
    {
        _modelClick.CountClick++;
        displayCount();
    }

}
