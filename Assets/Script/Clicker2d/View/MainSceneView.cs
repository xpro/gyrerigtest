﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneView : MonoBehaviour
{

    public Text textCount;
    public Button clickButton;

    private MainPresenter _mainPresenter;
    void Start()
    {
        _mainPresenter = new MainPresenter(this);
        clickButton.GetComponent<Button>().onClick.AddListener(() =>Click());
    }

    public void displayCount(int value)
    {
        textCount.text = value.ToString();
    }

    public void Click()
    {
        _mainPresenter.addClick();
    }

}
