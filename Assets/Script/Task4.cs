namespace Task4
{
    public struct ApiSetup<T>
    { }
    class Api
    {
        public ApiSetup<T> For<T>(T obj)
        {
            return new ApiSetup<T>();
        }
    }
    interface ISomeInterfaceA
    { }
    interface ISomeInterfaceB
    { }
    public class ObjectA : ISomeInterfaceA
    { }
    public class ObjectB : ISomeInterfaceB
    { }
    class SomeClass
    {
        public void Setup()
        {
            Api apiObject = new Api();

            apiObject.For(new ObjectA()).SetupObjectA();
            apiObject.For(new ObjectB()).SetupObjectB();
        }
    }

    public static class SetupObjectExtension
    {
        //Первый вариант
        public static void SetupObjectA(this ApiSetup<ObjectA> obj) 
        {
            
        }
        public static void SetupObjectB(this ApiSetup<ObjectB> obj)
        {
            
        }

        //Второй вариант
        public static void SetupObjectA1<T>(this ApiSetup<T> obj) where T : ObjectA
        {

        }

        public static void SetupObjectB1<T>(this ApiSetup<T> obj) where T : ObjectB
        {

        }

    }
}